import pytest
from validate_phone_number.main import validate_phone
from validate_phone_number.main import NotValidPhoneNumber
from phonenumbers import NumberParseException

# fixtures (pytest dokumentace)
@pytest.mark.parametrize(
    "phone",
    [
        "+420 775-021-425",
        "+420(775021425)",
        "+62(812)30746554",
        "+420737937772",
        "+6281230746554",
    ],
)
def test_valid_phone_number_is_valid(phone):
    validated = validate_phone(phone)
    assert isinstance(validated, str) and validated != ""


@pytest.mark.parametrize("phone", ["09999934014", "+420(111021425)"])
def test_invalid_phone_number_doesnt_parse(phone):
    with pytest.raises(NumberParseException):
        validate_phone(phone)


# @pytest.mark.parametrize("phone", ["+420(111021425)"])
# def test_invalid_phone_number_is_not_valid(phone):
#     with pytest.raises(NotValidPhoneNumber):
#         validate_phone(phone)
