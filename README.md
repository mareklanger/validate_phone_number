# README #
## Validate phone number 

### Function for phone number validation using phonenumbers.py library

##### NOTES by phonenumbers.py:
The method is quite lenient and looks for a number in the input text
(raw input) and does not check whether the string is definitely only a
phone number. To do this, it ignores punctuation and white-space, as
well as any text before the number (e.g. a leading "Tel: ") and trims
the non-number bits.  It will accept a number in any format (E164,
national, international etc), assuming it can be interpreted with the
defaultRegion supplied. It also attempts to convert any alpha characters
into digits if it thinks this is a vanity number of the type "1800
MICROSOFT".

##### INPUTS:
```
`phone_number` ->   preferable format: `f"+{country_code_prefix}{national_number}"` --> international format
`country` ->        `None` by default. 
                    Country (region) needs to be specified if `phone_number` does not contain `country_code_prefix`
```

##### OUTPUT:
```
String of validated `phone_number` as follows: `f"+{country_code_prefix}{national_number}"`
```

##### Exceptions:
###### `NumberParseException`
```
`NumberParseException` is throw if the number is not
considered to be a possible number. Note that validation of whether the
number is actually a valid number for a particular region is not
performed. This can be done separately with is_valid_number. 

`NumberParseException` if the string is not considered to be a viable
phone number (e.g.  too few or too many digits) or if no default
region was supplied and the number is not in international format
(does not start with +).
(taken from phonenumbers.py).
```

###### `NotValidPhoneNumber`
```
`NotValidPhoneNumber` is throw if parsed phone_number is not considered as valid
according to rules (e.g.  too few or too many digits) of specified region (country)
```