import phonenumbers
from phonenumbers import NumberParseException


class NotValidPhoneNumber(NumberParseException):
    pass


COUNTRY_CODE_MAPPING = {
    "Indonesia": "ID",
    "Philippines": "PH",
    "Singapore": "SG",
    "Czechia": "CZ",
    "India": "IN",
    "United States": "US",
    "Australia": "AU",
    "Vietnam": "VN",
    "China": "CN",
    "Hong Kong": "HK",
    "Hungary": "HU",
    "Malaysia": "MY",
    "Pakistan": "PK",
    "Poland": "PL",
    "Slovakia": "SK",
    "Thailand": "TH",
    "Turkey": "TR",
    "United Arab Emirates": "AE",
}


def validate_phone(
    phone_number, country=None, country_code_mapping=COUNTRY_CODE_MAPPING
):
    country = country_code_mapping.get(country)
    pn = phonenumbers.parse(phone_number, country)
    if phonenumbers.is_valid_number(pn):
        return f"+{pn.country_code}{pn.national_number}"
    else:
        raise NotValidPhoneNumber(
            NumberParseException, f"Number: {phone_number} is not valid"
        )

